import re

with open("minetest.conf", 'r') as conf_file:
    for line in conf_file.readlines():
        if re.match("^# [a-zA-Z]", line):
            splitted_line = line.split()
            print(f"set_config_value {splitted_line[1]} {splitted_line[1].upper()}")
        elif re.match("^##", line):
            print(f"\n{line}")

with open("settingtypes.txt", 'r') as vl_conf_file:
    for line in vl_conf_file.readlines():
        if re.match("^\[", line):
            print(f"\n## {line.strip().strip('[]')}\n")
        if re.match("^[a-z]", line):
            splitted_line = line.split()
            print(f"set_config_value {splitted_line[0]} {splitted_line[0].upper()}")
            
