# VoxeLibre Container

This is a Containerfile plus entrypoint for [VoxeLibre](https://git.minetest.land/VoxeLibre/VoxeLibre). The configuration file is
created via a script that reads environment variables. Most configuration options are supported. See the `entrypoint.sh`
for reference.(Will add this to the README later on)

## How to use

I publish images build from this to the [registry on codeberg](https://codeberg.org/Elouin/-/packages/container/voxelibre/)
linked to this repo. You can simply use these or build the image yourself(which
helps lower the load on codeberg).

### Persisting data

If you would like to keep the world data, you should mount a volume to `/var/lib/minetest/.minetest/worlds`.

### Configuration

The configuration is done via environment variables. They are named the same as
the configuration options, just in uppercase. So for example to change the port
the server is listening on, set the environment variable `PORT`.

### Podman example

First create a volume for the world data to live in.

```bash
podman volume create voxelibreworld
```

Then simply start the container.

```bash
podman run -v voxelibreworld:/var/lib/minetest/.minetest/worlds -p 30000:30000/udp codeberg.org/elouin/voxelibre:0.87.0
```

If you want to change some config option just add the corresponding env vars via
the `-e` flag, for example the port:

```bash
podman run -v voxelibreworld:/var/lib/minetest/.minetest/worlds -e PORT=1337 -p 1337:1337/udp codeberg.org/elouin/voxelibre:0.87.0
```

### Docker example

This is basically the same as with podman.
First create a volume for the world data to live in.

```bash
docker volume create voxelibreworld
```

Then simply start the container.

```bash
docker run -v voxelibreworld:/var/lib/minetest/.minetest/worlds -p 30000:30000/udp codeberg.org/elouin/voxelibre:0.87.0
```

If you want to change some config option just add the corresponding env vars via
the `-e` flag, for example the port:

```bash
docker run -v voxelibreworld:/var/lib/minetest/.minetest/worlds -e PORT=1337 -p 1337:1337/udp codeberg.org/elouin/voxelibre:0.87.0
```

### Guix

For guix add this to the services of your system configuration:

```scheme
(service oci-container-service-type
			(list
			  (oci-container-configuration
			    (image "codeberg.org/elouin/voxelibre:0.87.0")
			    (ports
			      '("30000:30000/udp"))
			    (volumes
			      '("voxelibreworld:/var/lib/minetest/.minetest/worlds"))
			    (environment
			      '(("SERVER_NAME" . "VoxeLibre Server")
      				("MOTD" . "Be kind and have fun."))))
```
